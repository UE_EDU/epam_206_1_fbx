# EPAM 206.1 Static Mesh Ingestion with Fbx – UE5

Developed with Unreal Engine 5

## Task Description: 

In this lesson you will learn how to import meshes with the correct scale, and with the instanced Materials. As well as how to fix the offset pivot point for the imported Static Mesh. And basics about Collisions and import multipart objects. 

### List of assets that should be present in the Project’s Content Folder: 

1. Folder with imported Zodiac_Inch.FBX with the correct scale 
2. Folder with the Master Material and another Folder with the Zodiac_MultiMatts and its Material Instances 
	a. Master Material 
	b. Raft Static Mesh 
	c. 5 Material Instances 
3. BOT_PivotBAD Static Mesh placed on a level with fixed Pivot 
4. Map for BOT_PivotBAD 
5. SimpleWall Static Mesh with the custom Collision 
6. Cessna Static Mesh with the hierarchy converted into a BP 
	a. 6 Materials 
	b. 7 Static Meshes 
	c. Blueprint for all SMs 

### Requirements for assets: 

1. Zodiac_Inch.FBX should be imported with Uniform Scale = 2.54 
2. Zodiac_MultiMatts  
	a. Master Material with parameters for Base Color, Metallic, and Roughness 
	b. Material Input Method set to "Create New Instanced Materials" and parented to newly created Master Material 
3. BOT_PivotBAD 
	a. Asset is placed in a map and its Pivot point should be fixed by placing it to the 0 coordinate on the map and parenting it to a Static Mesh 
4. SimpleWall 
	a. Mesh should have two Simple Box Collisions for both columns. Central ark shouldn’t have any collition! 
5. Cessna Plain 
	a. Mesh should be imported with the Hierarchy intact 
	b. A Blueprint with the correct hierarchy should be created out of the imported assets 